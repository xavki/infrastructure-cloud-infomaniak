%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 



-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Instance Installation

<br>

Doc : https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs


-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Instance Installation

<br>

Create our security groups - ssh from all 

  * just for our vpn to play ansible

  * you could whitelist your personal ip

```
resource "openstack_compute_secgroup_v2" "ssh" {
  name = "ssh-from-all"
  description = "ssh security group"
  rule {
    from_port = 22
    to_port = 22
    ip_protocol = "tcp"
    cidr = "0.0.0.0/0"  #personal ip
  }
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Instance Installation

<br>

Create our security groups - for our vpn service (openvpn)

  * you could whitelist your personal ip

```
resource "openstack_compute_secgroup_v2" "openvpn" {
  name = "openvpn"
  description = "openvpn security group"
  rule {
    from_port = 1194
    to_port = 1194
    ip_protocol = "udp"
    cidr = "0.0.0.0/0" #personal ip
  }
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Instance Installation

<br>

Create our security groups - to deploy other servers (ansible)

  * to deploy we'll use our vpn

```
resource "openstack_compute_secgroup_v2" "ssh-internal" {
  name = "ssh-from-internal"
  description = "ssh internal security group"
  rule {
    from_port = 22
    to_port = 22
    ip_protocol = "tcp"
    cidr = "10.0.1.0/24"
  }
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Instance Installation

<br>

Create our security groups - openbar tcp/udp internal ports

  * because consul and other services :)

  * doc : https://developer.hashicorp.com/consul/docs/install/ports

```
resource "openstack_compute_secgroup_v2" "all_internal" {
  name = "all_internal"
  description = "all_internal security group"
  rule {
    from_port = 1
    to_port = 65535
    ip_protocol = "tcp"
    cidr = "10.0.1.0/24"
  }
  rule {
    from_port = 1
    to_port = 65535
    ip_protocol = "udp"
    cidr = "10.0.1.0/24"
  }
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Instance Installation

<br>

Create our security groups - dedicated rules for our proxy http/https allowed

```
resource "openstack_compute_secgroup_v2" "proxy" {
  name = "proxy"
  description = "proxy security group"
  rule {
    from_port = 80
    to_port = 80
    ip_protocol = "tcp"
    cidr = "0.0.0.0/0"
  }
    rule {
    from_port = 443
    to_port = 443
    ip_protocol = "tcp"
    cidr = "0.0.0.0/0"
  }
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Instance Installation

<br>

Update our variable file - default instance name

```
variable "instance_name" {
  type 		= string
  default  	= "instance_name"
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Instance Installation

<br>

Update our variable file - default image ID

```
variable "instance_image_id" {
  type 		= string
  default  	= "cdf81c97-4873-473b-b0a3-f407ce837255"
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Instance Installation

<br>

Update our variable file - default instance flavor

```
variable "instance_flavor_name" {
  type 		= string
  default  	= "a1-ram2-disk20-perf1"
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Instance Installation

<br>

Update our variable file - default security group

```
variable "instance_security_groups" {
  type = list
  default = ["default"]
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Instance Installation

<br>

Update our variable file - default metadata

```
variable "metadatas" {
  type = map(string)
  default = {
    "environment" = "dev"
  }
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Instance Installation

<br>

Add new variable for our network

```
variable "network_external_name" {
  type 		= string
  default  	= "ext-floating1"
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Instance Installation

<br>

Get data for external_network and subnet

```
data "openstack_networking_subnet_ids_v2" "ext_subnets" {
  network_id = var.network_external_id
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Instance Installation

<br>

Get a floating ip in our external network

```
resource "openstack_networking_floatingip_v2" "floatip_1" {
  pool       = var.network_external_name
  subnet_ids = data.openstack_networking_subnet_ids_v2.ext_subnets.ids
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Instance Installation

<br>

Create our instance

```
resource "openstack_compute_instance_v2" "openvpn" {
  name            = "openvpn"
  image_id        = var.instance_image_id
  flavor_name     = var.instance_flavor_name
  metadata        = var.metadatas
  security_groups = ["default","ssh-from-all","openvpn"]
  key_pair = openstack_compute_keypair_v2.ssh_public_key.name
  network {
    name = var.network_internal_dev
  }
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Instance Installation

<br>

Associate our floating ip to our instance

```
resource "openstack_compute_floatingip_associate_v2" "fip_assoc" {
  floating_ip = openstack_networking_floatingip_v2.floatip_1.address
  instance_id = openstack_compute_instance_v2.instance.id
}
```

