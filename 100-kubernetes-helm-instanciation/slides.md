%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm instanciation

<br>

Imagine you need to deploy from 1 to N nginx instances ?

and you have a kubernetes cluster

Helm is your friend ;)


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm instanciation

<br>

Without Helm, we need :

  * a deployement

  * a config map (to edit nginx home page for example)

  * a service to route all trafic to pods

  * generaly we need an ingress... but not us... we have consul/traefik mode ;)


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm instanciation

<br>

A standalone deployement

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mynginx
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mynginx
  template:
    metadata:
      labels:
        app: mynginx
    spec:
      containers:
        - name: mynginx
          image: nginx:latest
          ports:
          - containerPort: 80
          volumeMounts:
            - name: html
              mountPath: "/usr/share/nginx/html/"
              readOnly: true
      volumes:
        - name: html
          configMap:
            name: mynginx
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm instanciation

<br>

The config map to change the home... of course not a real example

```
apiVersion: v1
kind: ConfigMap
metadata:
  name: mynginx
data:
  index.html: |
    Hello mynginx !!!
```

But we could mount a dedicated volume with statics

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm instanciation

<br>

And finaly the service with consul annotations :

```
apiVersion: v1
kind: Service
metadata:
  name: mynginx
  labels:
    app: mynginx
  annotations:
    consul.hashicorp.com/service-name: mynginx
    consul.hashicorp.com/service-sync: "true"
    consul.hashicorp.com/service-tags: |
      traefik.enable=true,
      traefik.http.routers.router-mynginx.entrypoints=http\,https,
      traefik.http.routers.router-mynginx.rule=Host(`mynginx.xavki.fr`),
      traefik.http.routers.router-mynginx.service=mynginx,
      traefik.http.routers.router-mynginx.middlewares=secure_headers@file\,to_https@file,
      traefik.http.routers.router-mynginx.tls.certresolver=certs_gen
spec:
  type: ClusterIP
  ports:
  - port: 80
    targetPort: 80
  selector:
    app: mynginx
```


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm instanciation

<br>

What about to chart :) ?

```
helm create instancier
find templates/ -name '*.yaml' -exec sed -i -e 's/mynginx/{{ .Release.Name }}/g' {} \;
helm install t1 instancier/
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm instanciation

<br>

Better to industrialize our job

```
find templates/ -name '*.yaml' -exec sed -i -e 's/.Release.Name/$instance/g' {} \;
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm instanciation

<br>

NOTES.txt

```
On est des fifoux on a d�ploy� les instances :

{{- range $instance := .Values.instances }}
- {{ $instance }}
{{- end }}
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm instanciation

<br>

values.yml

```
instances:
  - pierre
  - paul
  - jacques
  - jean
  - michel
  - leon
  - lea
  - amine
  - rabah
  - xavier
  - anthony
  - simon
  - casimir
  - quasimodo
  - emmanuel
  - nicolas
  - francois
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm instanciation

<br>

```
helm install i1 instancier --values=values.yaml
```