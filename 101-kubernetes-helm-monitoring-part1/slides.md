%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm - kube prometheus stack & fix

<br>

Kube prometheus stack installation

```
helm repo add prometheus https://prometheus-community.github.io/helm-charts
helm repo update
helm repo list
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm - kube prometheus stack & fix

<br>

Values file

```
grafana:
  enabled: true
prometheus:
  enabled: true
  prometheusSpec:
    enableAdminAPI: true
    retention: 4h
nodeExporter:
  enabled: false
```

```
helm install kube-prometheus-stack prometheus/kube-prometheus-stack -n monitoring --values values.yml
```


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm - kube prometheus stack & fix

<br>

```
kubectl get nodes -o jsonpath='{range .items[*]}{.status.addresses[?(@.type=="InternalIP")].address};{.spec.podCIDR}{"\n"}{end}'
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm - kube prometheus stack & fix

<br>

```
- name: Get node ip and their PodCidr
  hosts: k8sm1
  become: yes
  tasks:
    - name: execute kubectl
      shell: kubectl get nodes -o jsonpath='{range .items[*]}{.spec.podCIDR} {.status.addresses[?(@.type=="InternalIP")].address}{"\n"}{end}'
      register: __kubectl_output
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm - kube prometheus stack & fix

<br>

```
- name: Définir un fact personnalisé pour les routes
  set_fact:
    node_routes: "{{ __kubectl_output.stdout_lines | map('split', ' ') | map('join', ',gateway=') }}"

- name: Afficher les routes générées
  debug:
    var: node_routes
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm - kube prometheus stack & fix

<br>

```
- name: Run locally the openstack command
  hosts: localhost
  connection: local
  tasks:
    - name: Ajouter les routes sur le routeur OpenStack
      command: openstack router add route --route destination={{ item }} rt1
      with_items: "{{ hostvars['k8sm1']['node_routes'] }}"
```