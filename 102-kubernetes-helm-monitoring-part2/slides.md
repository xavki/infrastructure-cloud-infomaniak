%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm - kube prometheus stack & Storage

<br>

Some fixes : due to ubuntu upgrade

Checkout changes on vpn


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm - kube prometheus stack & Storage

<br>

But we need to persist prometheus storage

Doc : https://github.com/prometheus-community/helm-charts/blob/main/charts/kube-prometheus-stack/values.yaml#L3815

```
grafana:
  enabled: true
prometheus:
  enabled: true
  prometheusSpec:
    retention: 4h
    storageSpec:
      volumeClaimTemplate:
        spec:
          storageClassName: xxx
          resources:
            requests:
              storage: 10Gi
nodeExporter:
  enabled: false
```


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm - kube prometheus stack & Storage

<br>

```
helm repo add prometheus https://prometheus-community.github.io/helm-charts
helm repo update
helm install kube-prometheus-stack prometheus/kube-prometheus-stack -n monitoring --create-namespace --values values.yml
```


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm - kube prometheus stack & fix

<br>

We can use helm to install the cinder CSI

```
helm repo add cpo https://kubernetes.github.io/cloud-provider-openstack
helm repo update
helm install cinder-csi -n kube-system cpo/openstack-cinder-csi --values values.yml
```


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm - kube prometheus stack & fix

<br>

Add variables

```
kubernetes_openstack_username: ""
kubernetes_openstack_password: ""
kubernetes_openstack_domain_name: "default"
kubernetes_openstack_auth_url: "https://api.pub1.infomaniak.cloud/identity"
kubernetes_openstack_tenant_id: ""
kubernetes_openstack_region: ""
kubernetes_helm_consulsync_version: "0.45.0"
kubernetes_helm_cinder_version: "2.30.0"
kubernetes_helm_prometheus_stack_version: "62.3.1"
kubernetes_helm_prometheus_stack_retention: "4h"
kubernetes_helm_prometheus_stack_sc: "csi-cinder-sc-delete"
kubernetes_helm_prometheus_stack_storage_size: "10Gi"
kubernetes_helm_prometheus_prom_replicas: 1
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm - kube prometheus stack & fix

<br>

Define variable from environment variables

```
  vars:
    kubernetes_openstack_username: "{{ lookup('ansible.builtin.env', 'OS_USERNAME') }}"
    kubernetes_openstack_password: "{{ lookup('ansible.builtin.env', 'OS_PASSWORD') }}"
    kubernetes_openstack_domain_name: "{{ lookup('ansible.builtin.env', 'OS_USER_DOMAIN_NAME') }}"
    kubernetes_openstack_auth_url: "{{ lookup('ansible.builtin.env', 'OS_AUTH_URL') }}"
    kubernetes_openstack_tenant_id: "{{ lookup('ansible.builtin.env', 'OS_PROJECT_ID') }}"
    kubernetes_openstack_region: "{{ lookup('ansible.builtin.env', 'OS_REGION_NAME') }}"  
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm - kube prometheus stack & fix

<br>

Add cinder helm part :

```
- name: install cinder-csi
  include_tasks: install_cinder_csi.yml
```

```
- name: Add stable chart repo
  kubernetes.core.helm_repository:
    name: cpo
    repo_url: "https://kubernetes.github.io/cloud-provider-openstack"
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm - kube prometheus stack & fix

<br>

Add cinder helm part :

```
- name: Deploy cinder-csi
  kubernetes.core.helm:
    name: cinder-csi
    chart_ref: cpo/openstack-cinder-csi
    chart_version: "{{ kubernetes_helm_cinder_version }}"
    release_namespace: kube-system
    create_namespace: true
    values: "{{ lookup('template', 'csi-cinder-values.yml.j2') | from_yaml }}"
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm - kube prometheus stack & fix

<br>

And values file csi-cinder-values.yml.j2

```
secret:
  enabled: true
  name: cinder-csi-cloud-config
  create: true
  hostMount: false
  filename: cloud.conf
  name: cinder-csi-cloud-config
  data:
    cloud.conf: |-
      [Global]
      username = {{ kubernetes_openstack_username }}
      password = {{ kubernetes_openstack_password }}
      domain-name = {{ kubernetes_openstack_domain_name }}
      auth-url = {{ kubernetes_openstack_auth_url }}
      tenant-id = {{ kubernetes_openstack_tenant_id }}
      region = {{ kubernetes_openstack_region }}
csi:
  provisioner:
    topology: "false"
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm - kube prometheus stack & Storage

<br>

And now for kube-prometheus-stack

```
- name: install prometheus-stack
  include_tasks: install_prometheus_stack.yml
```

```
- name: Add stable chart repo
  kubernetes.core.helm_repository:
    name: prometheus
    repo_url: "https://prometheus-community.github.io/helm-charts"
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm - kube prometheus stack & Storage

<br>

```
- name: Deploy prometheus stack
  kubernetes.core.helm:
    name: kube-prometheus-stack
    chart_ref: prometheus/kube-prometheus-stack
    chart_version: "{{ kubernetes_helm_prometheus_stack_version }}"
    release_namespace: monitoring
    create_namespace: true
    values: "{{ lookup('template', 'prometheus-stack-values.yml.j2') | from_yaml }}"
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm - kube prometheus stack & Storage

<br>

And values for prometheus stack

```
grafana:
  enabled: true
prometheus:
  enabled: true
  prometheusSpec:
    retention: {{ kubernetes_helm_prometheus_stack_retention }}
    replicas: {{ kubernetes_helm_prometheus_prom_replicas }}
    storageSpec:
      volumeClaimTemplate:
        spec:
          storageClassName: {{ kubernetes_helm_prometheus_stack_sc }}
          resources:
            requests:
              storage: {{ kubernetes_helm_prometheus_stack_storage_size }}
nodeExporter:
  enabled: false
```
