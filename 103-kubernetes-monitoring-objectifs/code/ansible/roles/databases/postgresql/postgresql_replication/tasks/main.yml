---
# tasks file for roles/databases/postgresql/postgresql_replication
- name: Gather the package facts
  ansible.builtin.package_facts:
    manager: auto

- name: Check if postgresql is installed
  ansible.builtin.assert:
    that:
      - ansible_facts.packages['postgresql'] | length > 0
    fail_msg: "Failed : You need to add a role to install postgresql"

- name: Update the /etc/hosts
  lineinfile:
    dest: "/etc/hosts"
    regexp: ".*\t{{ item }}\t{{ item }}"
    line: "{{ hostvars[item]['ansible_host'] }}\t{{ item }}\t{{ item }}"
    state: present
  when: ansible_hostname != item
  loop: "{{ groups[postgresql_replication_group] }}"

- name: include wal_g if needed
  include_tasks: wal_g.yml
  when: postgresql_replication_wal_g_enabled and postgresql_replication_primary == ansible_hostname

- name: install repmgr
  ansible.builtin.apt:
    name: repmgr,python3-psycopg2
    state: present
    update_cache: true

- name: create repmgr user
  community.postgresql.postgresql_user:
    name: repmgr
    password: '{{ postgresql_replication_repmgr_password }}'
    role_attr_flags: REPLICATION,CREATEDB,CREATEROLE,SUPERUSER
  become_user: postgres

- name: "create database repmgr"
  community.postgresql.postgresql_db:
    name: repmgr
    owner: repmgr
  when: ansible_hostname == postgresql_replication_primary 
  become_user: postgres

- name: add pg_hba.conf
  template:
    src: pg_hba.conf.j2
    dest: "/etc/postgresql/{{ postgresql_replication_version_major }}/main/pg_hba.conf"
    mode: 0640
    owner: postgres
    group: postgres
  notify: reload_postgresql

- name: add pgpass for authentication
  template:
    src: pgpass.j2
    dest: "/var/lib/postgresql/.pgpass"
    mode: 0600
    owner: postgres
    group: postgres

- name: add replication.conf for replication settings
  template:
    src: replication.conf.j2
    dest: "/etc/postgresql/{{ postgresql_replication_version_major }}/main/conf.d/replication.conf"
    mode: 0640
    owner: postgres
    group: postgres
  notify: restart_postgresql

- name: "Flush handlers"
  meta: flush_handlers

- name: add /etc/default/repmgr settings
  template:
    src: repmgrd.j2
    dest: /etc/default/repmgrd
    mode: 0640
    owner: postgres
    group: postgres
  notify: restart_repmgr

- name: add repmgr.conf
  template:
    src: repmgr.conf.j2
    dest: /etc/repmgr.conf
    mode: 0640
    owner: postgres
    group: postgres
  notify: restart_repmgr

- name: "Flush handlers"
  meta: flush_handlers

- name: start repmgr if not already start
  service:
    name: repmgrd
    state: started
    enabled: yes

- name: check repmgr status
  shell: "repmgr cluster show"
  become_user: postgres
  changed_when: False
  ignore_errors: True
  register: __test_repmgr

- include_tasks: primary.yml
  when: ansible_hostname == postgresql_replication_primary and ansible_hostname not in __test_repmgr.stdout

- include_tasks: secondary.yml
  when: ansible_hostname != postgresql_replication_primary and ansible_hostname not in __test_repmgr.stdout

- name: push pg_rsync.sh
  template:
    src: pg_sync.sh.j2
    dest: /opt/
    mode: 750
    owner: root
    group: root

- name: add consul to sudoers for postgres user
  lineinfile:
    dest: /etc/sudoers
    state: present
    regexp: '^consul ALL = (postgres)'
    line: "consul ALL = (postgres) NOPASSWD: /usr/bin/repmgr"
    validate: 'visudo -cf %s'
  environment:
    PATH: /usr/sbin:/usr/local/sbin:/sbin

- name: install check script
  template:
    src: failover_postgresql.sh.j2
    dest: /opt/failover_postgresql.sh
    owner: root
    group: root
    mode: 0755

