%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Kubernetes monitoring & Objectifs

<br>

Move to kubernetes ?

  * keep prometheus ?
  * add the monitoring stack : progressive methode (remote write, grafana chart in kube prometheus stack)
  * easier thanks to consul and its DNS
  * victoria metrics migration
  * our dashboards
  * define requests and limits why ?
  * but it's better to use a dedicated gitops for kubernetes ? argocd/helm
  * postgresql / keycloak / mattermost
  * maybe gitlab

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Kubernetes monitoring & Objectifs

<br>

Push our metrics on victoriametrics to use prometheus like an agent

```
prometheus:
  enabled: true
  prometheusSpec:
    remoteWrite:
      - url: http://vmetrics.service.xavki.consul:8428/api/v1/write
    retention: {{ kubernetes_helm_prometheus_stack_retention }}
    replicas: {{ kubernetes_helm_prometheus_prom_replicas }}
    storageSpec:
      volumeClaimTemplate:
        spec:
          storageClassName: {{ kubernetes_helm_prometheus_stack_sc }}
          resources:
            requests:
              storage: {{ kubernetes_helm_prometheus_stack_storage_size }}
nodeExporter:
  enabled: false
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Kubernetes monitoring & Objectifs

<br>

Configure the grafana in kubernetes

```
grafana:
  enabled: true
  additionalDataSources:
  - name: VictoriaMetrics
    url: http://vmetrics.service.xavki.consul:8428
    type: prometheus
    access: proxy
    isDefault: true
  deleteDatasources: 
  - name: Prometheus
    orgId: 1
  sidecar:
    datasources:
      enabled: true
      defaultDatasourceEnabled: false
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Kubernetes monitoring & Objectifs

<br>

Configure the grafana in kubernetes

```
  service:
    annotations:
      consul.hashicorp.com/service-name: graf
      consul.hashicorp.com/service-sync: "true"
      consul.hashicorp.com/service-tags: |
        traefik.enable=true,
        traefik.http.routers.router-graf.entrypoints=http\,https,
        traefik.http.routers.router-graf.rule=Host(`gx.xavki.fr`),
        traefik.http.routers.router-graf.service=graf,
        traefik.http.routers.router-graf.middlewares=secure_headers@file\,to_https@file,
        traefik.http.routers.router-graf.tls.certresolver=certs_gen
```

Note : check consul configuration

