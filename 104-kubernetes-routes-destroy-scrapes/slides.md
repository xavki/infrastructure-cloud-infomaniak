%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Kubernetes monitoring part2

<br>

Improvement :

  * on destroy : destroy all routes on our router (useful if we keep the router)

Move :

  * add previous scrapes on our prometheus (switch from vmagent)


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Kubernetes monitoring part2


<br>

On destroy clean routes of our router

  * create a new playbook file

```
- name: Clean route on the router
  hosts: localhost
  connection: local
  tasks:
    - name: Add k8s routes to our router
      command: openstack router set rt1 --no-route
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Kubernetes monitoring part2


<br>

On destroy clean routes of our router

  * add a new terrafomr resources which runs only on destroy

```
resource "null_resource" "destroy_routes" {

  triggers = {
    ansible_env_vars  = var.ANSIBLE_ENV_VARS
    ansible_command   = var.ANSIBLE_COMMAND
    default_user      = var.default_user
    ansible_options   = var.ANSIBLE_OPTIONS
  }

  provisioner "local-exec" {
    command = <<-EOT
      ${self.triggers.ansible_env_vars} ${self.triggers.ansible_command} ${self.triggers.default_user} ${self.triggers.ansible_options} ../../ansible/infrastructure_kubernetes_remove_routes.yml;
    EOT
    when = destroy
  }
}
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Kubernetes monitoring part2


<br>

Add previous scrapes in our prometheus configuration

  * some cleans and choices (listen all if you want to scrape)

```
kubeControllerManager:
  enabled: false
kubeScheduler:
  enabled: false
kubeEtcd:
  enabled: false
kubeProxy:
  enabled: false
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Kubernetes monitoring part2


<br>

Add previous scrapes in our prometheus configuration

  * some cleans and choices (listen all if you want to scrape)

```
  prometheusSpec:
...
    additionalScrapeConfigs:
      - job_name: node_exporter
        consul_sd_configs:
        - server: 'consul.service.xavki.consul:8500'
          services:
...
```

Note : remove param

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Kubernetes monitoring part2


<br>

Finaly grafana persistence

```
persistence:
  enabled: true
  storageClassName: "storageClassName"
  size: 20Gi
```