%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaMetrics operator : what is it ?

<br>

What is an operator ?

  * an automatic tool

  * integrated at kubernetes

  * create your own kubernetes object

  * to handle your own logical component : as pods, deployments...

  * handle the lifecycle of this resource

-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaMetrics operator : what is it ?

<br>

Operator = 

  * CRD (Custom Resource Definition) : description of this new kind of object

  * Controller : what kubernetes need to do (maintain state) when we create an instance of this resource
      default : deployment, replicaset, node, servioces, statefulset, job, cronjob...

Reconciliation loop : in 3 steps

  * desired state : from users or controllers

  * current state : check the resource permanently

  * reconciliation : take action if needed

-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaMetrics operator : what is it ?

<br>

Victoria Metrics operator example :

    vlogs.operator.victoriametrics.com                 
    vmagents.operator.victoriametrics.com              
    vmalertmanagerconfigs.operator.victoriametrics.com   
    vmalertmanagers.operator.victoriametrics.com       
    vmalerts.operator.victoriametrics.com                
    vmauths.operator.victoriametrics.com                 
    vmclusters.operator.victoriametrics.com              
    vmnodescrapes.operator.victoriametrics.com         
    vmpodscrapes.operator.victoriametrics.com          
    vmprobes.operator.victoriametrics.com                
    vmrules.operator.victoriametrics.com                 
    vmscrapeconfigs.operator.victoriametrics.com         
    vmservicescrapes.operator.victoriametrics.com        
    vmsingles.operator.victoriametrics.com               
    vmstaticscrapes.operator.victoriametrics.com         
    vmusers.operator.victoriametrics.com               

-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaMetrics operator : what is it ?

<br>

Victoria Metrics - cluster mode

  * vmstorage

  * vminsert

  * vmselect

  * vmauth

  * vmagent
  
  * vmalert

  * alertmanager

