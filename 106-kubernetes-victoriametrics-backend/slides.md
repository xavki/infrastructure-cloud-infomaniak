%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaMetrics cluster with its operator

<br>

Change the helm version 

```
kubernetes_tooling_helm_version: 3.16.1
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaMetrics cluster with its operator

<br>

Variables

```
kubernetes_helm_prometheus_prom_remote_write: "http://vminsert-victoriametrics.monitoring.svc.cluster.local:8480/insert/0/prometheus/api/v1/write"
kubernetes_helm_prometheus_grafana_datasource: "http://vmselect-victoriametrics.monitoring.svc.cluster.local:8481/select/0/prometheus"
kubernetes_helm_prometheus_stack_grafana_sc: "csi-cinder-sc-delete"
kubernetes_helm_vmcluster_name: "victoriametrics"
kubernetes_helm_vmoperator_stack_version: "0.34.8"
kubernetes_helm_vmcluster_namespace: "monitoring"
kubernetes_helm_vmcluster_vmselect_replicas: "2"
kubernetes_helm_vmcluster_vminsert_replicas: "2"
kubernetes_helm_vmcluster_vmstorage_replicas: "2"
kubernetes_helm_vmcluster_vmstorage_retention: "1y"
kubernetes_helm_vmcluster_vmselect_size: "2Gi"
kubernetes_helm_vmcluster_vmstorage_size: "20Gi"
kubernetes_helm_vmcluster_vmstorage_storageclass: "csi-cinder-sc-delete"
kubernetes_helm_vmcluster_vmselect_storageclass: "csi-cinder-sc-delete"
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaMetrics cluster with its operator

<br>

Add kubernetes python library

```
name: python3-yaml, python3-kubernetes
```


Add sub tasks

```
- name: install victoriametrics cluster
  include_tasks: install_vmstack.yml
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaMetrics cluster with its operator

<br>

Add the victoriametrics repository

```
- name: Add stable chart repo
  kubernetes.core.helm_repository:
    name: vm
    repo_url: "https://victoriametrics.github.io/helm-charts/"
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaMetrics cluster with its operator

<br>

Add the operator

```
- name: Deploy victoriametrics operator
  kubernetes.core.helm:
    name: vmoperator
    chart_ref: vm/victoria-metrics-operator
    chart_version: "{{ kubernetes_helm_vmoperator_stack_version }}"
    release_namespace: kube-vmetrics
    create_namespace: true
    wait: True
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaMetrics cluster with its operator

<br>

Install vmcluster

```
- name: Deploy VMCluster
  kubernetes.core.k8s:
    state: present
    definition: "{{ lookup('template', 'vmcluster.yml.j2') | from_yaml }}"
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaMetrics cluster with its operator

<br>

The vmcluster manifest

```
apiVersion: operator.victoriametrics.com/v1beta1
kind: VMCluster
metadata:
  name: {{ kubernetes_helm_vmcluster_name }}
  namespace: {{ kubernetes_helm_vmcluster_namespace }}
spec:
  retentionPeriod: "{{ kubernetes_helm_vmcluster_vmstorage_retention }}"
  replicationFactor: 2
  vmstorage:
    replicaCount: {{ kubernetes_helm_vmcluster_vmstorage_replicas }}
    storageDataPath: "/vm-data"
    storage:
      volumeClaimTemplate:
        metadata:
          annotations:
            operator.victoriametrics.com/pvc-allow-volume-expansion: "false"
        spec:
          storageClassName: {{ kubernetes_helm_vmcluster_vmstorage_storageclass }}
          resources:
            requests:
              storage: {{ kubernetes_helm_vmcluster_vmstorage_size }}
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaMetrics cluster with its operator

<br>

Vmcluster suite

```
  vmselect:
    replicaCount: {{ kubernetes_helm_vmcluster_vmselect_replicas }}
    cacheMountPath: "/select-cache"
    storage:
      volumeClaimTemplate:
        metadata:
          annotations:
            operator.victoriametrics.com/pvc-allow-volume-expansion: "false"
        spec:
          storageClassName: {{ kubernetes_helm_vmcluster_vmselect_storageclass }}
          resources:
            requests:
              storage: {{ kubernetes_helm_vmcluster_vmselect_size }}
  vminsert:
    replicaCount: {{ kubernetes_helm_vmcluster_vminsert_replicas }}
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaMetrics cluster with its operator

<br>

Example : http://10.200.2.5:8481/select/0/