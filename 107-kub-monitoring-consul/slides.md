%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : consul-exporter & service monitor

<br>

Purposes :

  * service monitor

  * migrate consul exporter

  * add consul dashboard

-----------------------------------------------------------------------------------------------------------

# Kubernetes : consul-exporter & service monitor

<br>

Service Monitor ??

  * kubernetes services dedicated to metric routes

  * avoid to edit prometheus configuration (easy to add it in helm)

  * provide by prometheus operator

  * detected by prometheus controller with its labels

-----------------------------------------------------------------------------------------------------------

# Kubernetes : consul-exporter & service monitor

<br>

Consul exporter installation

```
helm install -n monitoring consul-exporter prometheus/prometheus-consul-exporter --values values-consul-exporter.yaml 
```


-----------------------------------------------------------------------------------------------------------

# Kubernetes : consul-exporter & service monitor

<br>

Consul exporter installation - chart release

```
- name: Deploy consul exporter
  kubernetes.core.helm:
    name: consul-exporter
    chart_ref: prometheus/prometheus-consul-exporter
    chart_version: "{{ kubernetes_helm_consul_exporter_version }}"
    release_namespace: monitoring
    create_namespace: true
    values: "{{ lookup('template', 'consul-exporter.yaml.j2') | from_yaml }}"
```

```
kubernetes_helm_consul_exporter_version: "1.0.0"
kubernetes_helm_consul_exporter_server: "consul.service.consul:8500"
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : consul-exporter & service monitor

<br>

Consul exporter installation - values

```
replicaCount: 1
consulServer: {{ kubernetes_helm_consul_exporter_server }}
serviceMonitor:
  enabled: true
  interval: 10s
  telemetryPath: /metrics
  labels:
    release: kube-prometheus-stack
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : consul-exporter & service monitor

<br>

Add dashboard

```
- name: Deploy grafana dashboards
  kubernetes.core.k8s:
    state: present
    definition: "{{ lookup('file', 'dashboards-consul.yml') | from_yaml }}"
```



