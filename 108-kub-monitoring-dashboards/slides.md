%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : grafana dashboards

<br>

Add a dashboard provider (allows us to store dahboards per directory)

```
grafana:
  enabled: true
  dashboardProviders:
    dashboardproviders.yaml:
      apiVersion: 1
      providers:
        - name: 'default'
          orgId: 1
          folder: 'default'
          type: file
          disableDeletion: true
          editable: true
          options:
            path: /var/lib/grafana/dashboards/default
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : grafana dashboards

<br>

Use the provider name as key and add a dashboards by url

```
  dashboards:
    default:
      node-exporter:
        datasource: VictoriaMetrics
        url: https://raw.githubusercontent.com/rfrail3/grafana-dashboards/master/prometheus/node-exporter-full.json
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : grafana dashboards

<br>

Push all other dashboards

```
- name: push dashboards configuration
  kubernetes.core.k8s:
    state: present
    definition: "{{ lookup('file', 'dashboards/' + item | basename ) | from_yaml  }}"
  with_fileglob:
  - files/dashboards/*.yml
```


-----------------------------------------------------------------------------------------------------------

# Kubernetes : grafana dashboards

<br>

Change all dashboards

```
kind: ConfigMap
metadata:
  name: grafana-dashboard-errorlog
  namespace: monitoring
  labels:
    apps: prometheus-operator-grafana
    grafana_dashboard: "1"
data:
  errorlog.json: |
```