%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaLogs to replace Loki

<br>

Purpose :

  * why ? just for fun and taht allows you to compare

  * we already have vlogs crd

  * loki vs vlogs : no S3, no clustering (multiple backends), grafana links

-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaLogs to replace Loki

<br>

Just a new resource

```
- name: Deploy VLogs
  kubernetes.core.k8s:
    state: present
    definition: "{{ lookup('template', 'vlogs-values.yml.j2') | from_yaml }}"
```

kubernetes_helm_vmcluster_vlogs_retention: "1y"
kubernetes_helm_vmcluster_vlogs_size: "30Gi"
kubernetes_helm_vmcluster_vlogs_storageclass: "csi-cinder-sc-delete"

-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaLogs to replace Loki

<br>

The template vlogs-values.yml.j2

```
apiVersion: operator.victoriametrics.com/v1beta1
kind: VLogs
metadata:
  name: vlogs
  namespace: monitoring
spec:
  retentionPeriod: "{{ kubernetes_helm_vmcluster_vlogs_retention }}"
  replicaCount: 1
  storage:
    storageClassName: "{{ kubernetes_helm_vmcluster_vlogs_storageclass }}"
    accessModes: 
      - "ReadWriteOnce"
    resources:
      requests:
        storage: "{{ kubernetes_helm_vmcluster_vlogs_size }}"
```

Note : if needed, add a service to add annotations

-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaLogs to replace Loki

<br>

Change the vector configuration (use condul dns)

```
endpoint: "http://vlogs-vlogs.service.consul:9428/insert/loki/"
path: /api/v1/push?_msg_field=message.message&_time_field=timestamp&_stream_fields=source
compression: gzip
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaLogs to replace Loki

<br>

Add datasource in grafana chart

```
- name: VictoriaLogs
  url: http://vlogs-vlogs.service.consul:9428
  type: victorialogs-datasource
  access: proxy
  isDefault: false
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaLogs to replace Loki

<br>

Add grafana settings to allow victorialogs as datasource

```
env:
  GF_PLUGINS_ALLOW_LOADING_UNSIGNED_PLUGINS: "victorialogs-datasource"
plugins:
  - https://github.com/VictoriaMetrics/victorialogs-datasource/releases/download/v0.5.0/victorialogs-datasource-v0.5.0.zip;victorialogs-datasource
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : VictoriaLogs to replace Loki

<br>

But ??!!

https://docs.victoriametrics.com/operator/api/#vlogs

```
instance: "traefik1" AND type: "traefik_access" 
instance: "traefik1" AND type: "traefik_access"  | unpack_json fields (OriginStatus)
instance: "traefik1"  AND type: "traefik_access" | unpack_json | entryPointName: "https"
instance: "traefik1"  AND type: "traefik_access" | unpack_json fields(entryPointName,OriginStatus)  | entryPointName: "https"
```
