%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 



-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation - part 2


<br>

openvpn

    server ( CA + cert + network )  >  client ( cert + interface + routes )

<br>

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation


<br>

Installation Openvpn Server step by step

  * installation packages

  * generate a CA (cert/key)

  * create server cert/key

  * build certificate revocate list (crl)

  * generate tls auth key/cert

  * configure the server

  * add iptables rules and persist its

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation


<br>

Check if tls-auth key exists

```
- name: check if ta key file already.stat.exists
  stat:
    path: "{{ ansible_env.HOME }}/openvpn-ca/pki/ta.key"
  register: __check_openvpn_ta_key_file_present
```

If not

```
- name: "key generation"
  shell: >
    source vars;
    openvpn --genkey --secret pki/ta.key;
  args: 
    chdir: "{{ ansible_env.HOME }}/openvpn-ca/"
    executable: /bin/bash
  when: __check_openvpn_ta_key_file_present.stat.exists == false
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation


<br>

Move all key and cert into /etc/openvpn

```
- name: "Copy key and certificates to /etc/openvpn"
  copy:
    remote_src: yes
    src: "{{ ansible_env.HOME }}/openvpn-ca/pki/{{ item }}"
    dest: "/etc/openvpn/"
    owner: root
    group: root
    mode: 0600
  loop:
    - "ca.crt"
    - "private/ca.key"
    - "private/server_{{ vpn_server_name }}.key"
    - "issued/server_{{ vpn_server_name }}.crt"
    - "ta.key"
    - "crl.pem"
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation


<br>

Check if conf file already exists


```
- name: check if conf file already.stat.exists
  stat:
    path: /etc/openvpn/server.conf
  register: __check_openvpn_conf_file_present
```

If not generate it

```
- name: generate server.conf from sample config
  become: true
  copy:
    remote_src: yes
    src: /usr/share/doc/openvpn/examples/sample-config-files/server.conf
    dest: /etc/openvpn/server.conf
  when: __check_openvpn_conf_file_present.stat.exists == false
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation

<br>

Adapt configuration

```
- name: Adjust OpenVPN server configuration
  lineinfile:
    dest: "/etc/openvpn/server.conf"
    regexp: "^{{ item.regex | regex_escape() }}"
    line: "{{ item.value }}"
  loop:
    - { regex: ';user nobody', value: 'user nobody' }
    - { regex: ';group nogroup', value: 'group nogroup' }
    - { regex: ';push "redirect-gateway def1 bypass-dhcp"', value: 'push "redirect-gateway def1 bypass-dhcp"' }
    - { regex: 'cert server.crt', value: 'cert server_{{ vpn_server_name }}.crt' }
    - { regex: 'key server.key', value: 'key server_{{ vpn_server_name }}.key' }
    - { regex: ';topology subnet', value: 'topology subnet' }
    - { regex: 'dh dh2048.pem', value: 'dh none' }
    - { regex: ';compress lz4-v2', value: 'compress lzo' }
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation

<br>

Adapt configuration 

```
- name: add some secure lines
  lineinfile:
    dest: "/etc/openvpn/server.conf"
    line: "{{ item }}"
  loop:
    - "ecdh-curve prime256v1"
    - "auth SHA256"
    - "cipher AES-128-GCM"
    - "ncp-ciphers AES-128-GCM"
    - "tls-server"
    - "tls-version-min 1.2"
    - "tls-cipher TLS-ECDHE-ECDSA-WITH-AES-128-GCM-SHA256"
    - "client-config-dir /etc/openvpn/ccd"
    - "status /var/log/openvpn/status.log"
    - "proto udp4"
    - "verb 3"
    - "log-append /var/log/openvpn.log"
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation

<br>

Adapt sysctl for ip forwarding

```
- name: Configuration IP forwarding
  become: true
  sysctl:
    name: net.ipv4.ip_forward
    value: "1"
    state: present
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation

<br>

Create a directory for ccd

```
- name: ensure to have ccd directory
  file:
    path: /etc/openvpn/ccd
    state: directory
    owner: root
    group: root
    mode: 0600
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation

<br>

Iptables rules postrouting

```
- name: add iptables rule postrouting
  iptables:
    table: nat
    chain: POSTROUTING
    rule_num: "1"
    source: 10.8.0.0/24
    out_interface: ens3
    jump: MASQUERADE
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation

<br>

Iptables rules input for tun0

```
- name: add iptables rule input tun0
  iptables:
    chain: INPUT
    rule_num: "1"
    in_interface: tun0
    jump: ACCEPT 
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation

<br>

Iptables rules forward

```
- name: add iptables rule forward tun0
  iptables:
    chain: FORWARD
    rule_num: "1"
    in_interface: ens3
    out_interface: tun0
    jump: ACCEPT 
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation

<br>

Iptables rules forward

```
- name: add iptables rule forward tun0
  iptables:
    chain: FORWARD
    rule_num: "1"
    in_interface: tun0
    out_interface: ens3
    jump: ACCEPT
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation

<br>

Iptables rules input for our interface with udp protocol

```
- name: add iptables rule input port openvpn
  iptables:
    chain: INPUT
    rule_num: "1"
    in_interface: ens3  
    jump: ACCEPT 
    protocol: udp
    destination_port: "1194"
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation

<br>

Start the server

```
- name: Start openvpn systemd service
  become: true
  systemd:
    name: openvpn@server
    state: started
    daemon_reload: yes
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Openvpn server installation

<br>

Persist iptables rules

```
- name: save iptables rules to persist them
  community.general.iptables_state:
    state: saved
    path: /etc/iptables/rules.v4
```

Note : ansible-galaxy collection install community.general
