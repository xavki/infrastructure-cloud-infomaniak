%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Inventaire Dynamique

<br>

Documentation

https://docs.ansible.com/ansible/latest/collections/openstack/cloud/openstack_inventory.html


-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Inventaire Dynamique

<br>

Create a openstack.yml file

```
plugin: openstack.cloud.openstack
cache: yes
all_projects: false
expand_hostvars: true
fail_on_errors: true
strict: true
private: true
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Inventaire Dynamique

<br>

Source your openrc.sh and

```
ansible-inventory -i openstack.yml --list
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Inventaire Dynamique

<br>

Metadatas usage

```
  metadatas                   = {
    environment          = "dev",
    app = "consul"
  }
```

Usage

```
ansible -i openstack.yml meta-app_consul -u xavki -m ping
ansible -i openstack.yml meta-app_consul -u xavki -b -m apt -a 'name=nginx state=latest'
```