%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Installation & Configuration ??

<br>

0. Change proxy rules...

1. Create dns or nip.io or direct ip with specific route...

2. Create instance

3. install traefik

4. configure static configuration

5. configure dynamix configuration

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Installation & Configuration ??

<br>

Create a dedicated instance for traefik :
  * with public ip ;)

```
module "traefik" {
  source                      = "../modules/instance"
  instance_count              = 1
  instance_name               = "traefik"
  instance_key_pair           = "default_key"
  instance_security_groups    = ["consul", "ssh-internal", "default","node_exporter","proxy"]
  instance_network_internal   = var.network_internal_dev
  instance_ssh_key = var.ssh_public_key_default_user
  instance_default_user = var.default_user
  instance_network_external_name = var.network_external_name
  instance_network_external_id  = var.network_external_id
  public_floating_ip = true
  metadatas                   = {
    environment          = "dev",
    app         = "proxy"
  }
}
```

Note : change secgroup_all_internal_rule_http_v4 !!!!

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Installation & Configuration ??

<br>

Create directories

```
mkdir -p /etc/traefik/ /var/log/traefik/
```

Download & install binary

```
wget https://github.com/traefik/traefik/releases/download/v2.11.0/traefik_v2.11.0_linux_amd64.tar.gz
tar xzvf traefik_v2.11.0_linux_amd64.tar.gz
mv traefik /usr/local/bin/traefik && chmod +x /usr/local/bin/traefik
```

Configuration - logs

```
[log]
  level = "INFO"
  filePath = "/var/log/traefik/traefik.log"
  format = "json"
[accessLog]
  filePath =  "/var/log/traefik/access-traefik.log"
  format = "json" 
```

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Installation & Configuration ??

<br>

Configuration - hot reload & dashboard

```
[file]
watch = true

[api]
  dashboard = true
  insecure = false
```

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Installation & Configuration ??

<br>

Configuration - logs

```
[entryPoints]
  [entryPoints.api]
    address = ":8080"
    [entryPoints.api.proxyProtocol]
      trustedIPs = ["127.0.0.1/32", "10.0.1.0/24"]
  [entryPoints.metrics]
    address = ":9200"
```

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Installation & Configuration ??

<br>

Configuration - static config

```
[providers.file]
  directory = "/etc/traefik/"             
  watch = true
```

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Installation & Configuration ??

<br>

Configuration - metrics

```
[metrics]
  [metrics.prometheus]
    entryPoint = "metrics"
    buckets = [0.1,0.3,1.2,5.0]
    addEntryPointsLabels = true
    addRoutersLabels = true
    addServicesLabels = true
```

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Installation & Configuration ??

<br>

Dynamic config - router

```
[http]
  [http.routers]
    [http.routers.api]
      rule = "PathPrefix(`/api`, `/dashboard`)"
      entryPoints = ["api"]
      service = "api@internal"
      middlewares = ["auth"]
```

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Installation & Configuration ??

<br>

Dynamic config - middlewares

* Generate a password

```
sudo apt install apache2-utils
htpasswd -nB password
```

* Create the middleware

```
  [http.middlewares]
    [http.middlewares.auth.basicAuth]
      users = [
      "xavki:$apr1$rnqfuhkt$u7ubv2bBKQYNscWTw2xy./"
      ]
    [http.middlewares.allow_ip.ipAllowList]
      sourceRange = ["127.0.0.1/32", "10.0.1.0/24"]
    [http.middlewares.gzip.compress]
```

Note : check with open security group & allow_ip

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Installation & Configuration ??

<br>

Launch traefik

```
/usr/local/bin/traefik --configfile=/etc/traefik/traefik.toml
```
