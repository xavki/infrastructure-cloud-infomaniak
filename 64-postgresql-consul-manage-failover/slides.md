%title: Infra Cloud Infomaniak
%author: xavki


██████╗  ██████╗ ███████╗████████╗ ██████╗ ██████╗ ███████╗███████╗ ██████╗ ██╗     
██╔══██╗██╔═══██╗██╔════╝╚══██╔══╝██╔════╝ ██╔══██╗██╔════╝██╔════╝██╔═══██╗██║     
██████╔╝██║   ██║███████╗   ██║   ██║  ███╗██████╔╝█████╗  ███████╗██║   ██║██║     
██╔═══╝ ██║   ██║╚════██║   ██║   ██║   ██║██╔══██╗██╔══╝  ╚════██║██║▄▄ ██║██║     
██║     ╚██████╔╝███████║   ██║   ╚██████╔╝██║  ██║███████╗███████║╚██████╔╝███████╗
╚═╝      ╚═════╝ ╚══════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚══════╝╚══════╝ ╚══▀▀═╝ ╚══════╝



-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Consul to manage failover


<br>

Purpose :

  * adapt our postgresql replication role

  * allow consul to chec which server is the primary


-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Consul to manage failover


<br>

Check that postgresql is installed

```
- name: Gather the package facts
  ansible.builtin.package_facts:
    manager: auto

- name: Check if postgresql is installed
  ansible.builtin.assert:
    that:
      - ansible_facts.packages['postgresql'] | length > 0
    fail_msg: "Failed : You need to add a role to install postgresql"
```


-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Consul to manage failover


<br>

Allow consul to use postgresql user without password for repmgr commands

```
- name: add consul to sudoers for postgres user
  lineinfile:
    dest: /etc/sudoers
    state: present
    regexp: '^consul ALL = (postgres)'
    line: "consul ALL = (postgres) NOPASSWD: /usr/bin/repmgr"
    validate: 'visudo -cf %s'
  environment:
    PATH: /usr/sbin:/usr/local/sbin:/sbin
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Consul to manage failover


<br>

Install our failover script

```
- name: install check script
  template:
    src: failover_postgresql.sh.j2
    dest: /opt/failover_postgresql.sh
    owner: root
    group: root
    mode: 0755
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Consul to manage failover


<br>

Add the template script

```
#!/bin/bash
  
standby_primary=`sudo -u postgres repmgr cluster show 2>/dev/null | awk -F '|' '$3 ~ "standby" && $4 ~ ".* as primary" {print $2}' | tr -d "\"" | wc -l`
primary_primary=`sudo -u postgres repmgr cluster show 2>/dev/null | awk -F '|' -v host="${HOSTNAME}" '$2 ~ host && $3 ~ "primary" && $4 ~ "\* running" {print $2}' | tr -d "\"" | tr -d " "`

if [[ "$standby_primary" == "0"  && "$primary_primary" == "${HOSTNAME}" ]];then
  echo 0
  exit 0
else
  echo 2
  exit 2
fi
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Consul to manage failover


<br>

Change our consul services role to allow us to use args check (need to have a list)

```
    "checks": [{
      "id": "{{ item.name }}",
      {% if item.type == "args"%}
      "{{ item.type }}": ["{{ item.check_target }}"],
      {% else %}
      "{{ item.type }}": "{{ item.check_target }}",
      {% endif %}
      "interval": "{{ item.interval }}"
    }]
```

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Consul to manage failover


<br>

Add consul service

```
consul_services:
  - {
    name: "postgresql_primary",
    type: "args",
    check_target: "/opt/failover_postgresql.sh",
    interval: "10s",
    port: 5432
    }
  - {
    name: "postgresql",
    type: "tcp",
    check_target: "127.0.0.1:5432",
    interval: "10s",
    port: 5432      
    }
```


-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : Consul to manage failover


<br>

Add consul service

```
, service_name!~"postgresql_.*"
```
