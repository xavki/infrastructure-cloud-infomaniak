%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗███████╗██╗   ██╗ ██████╗██╗      ██████╗  █████╗ ██╗  ██╗
██║ ██╔╝██╔════╝╚██╗ ██╔╝██╔════╝██║     ██╔═══██╗██╔══██╗██║ ██╔╝
█████╔╝ █████╗   ╚████╔╝ ██║     ██║     ██║   ██║███████║█████╔╝ 
██╔═██╗ ██╔══╝    ╚██╔╝  ██║     ██║     ██║   ██║██╔══██║██╔═██╗ 
██║  ██╗███████╗   ██║   ╚██████╗███████╗╚██████╔╝██║  ██║██║  ██╗
╚═╝  ╚═╝╚══════╝   ╚═╝    ╚═════╝╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝



-----------------------------------------------------------------------------------------------------------

# Keycloak : What is it ?

<br>

SSO : Single Sign-On = unified authentication (authorization)

Official Website : https://www.keycloak.org/
Github : https://github.com/keycloak/keycloak

Implemented protocols: OpenID Connect, OAuth2.0 and SAML


Flow : user > apps > keycloak > apps

Authentication vs Authorization

Naitsec : 

  * keycloak : https://www.youtube.com/playlist?list=PLtBjphVrKuHW6U1P_qKEAHwVeTLDDiUNG
  * OIDC : https://www.youtube.com/playlist?list=PLtBjphVrKuHUItsxaphfY_0cH997fa9xe

Julien Topçu (OAuth2.1) : https://www.youtube.com/watch?v=YdShQveywpo

-----------------------------------------------------------------------------------------------------------

# Keycloak : What is it ?

<br>

Realm : default master

  * users & groups

  * client

  * roles

  * sessions

  * events

  * providers

  * themes

  * auth management

-----------------------------------------------------------------------------------------------------------

# Keycloak : What is it ?

<br>

Client :

  * one application

  * specific urls and redirect url

-----------------------------------------------------------------------------------------------------------

# Keycloak : What is it ?

<br>

Users :

  * people who need to connect

  * have some atttributes: email, firstname...

  * can be added to groups ou roles

<br>

Groups :

  * allow us to handle many user at the same time

Roles :

  * allow you to manage user permission level (admin, viewer...) 

  * mapping role with users and/or with clients

  * client roles or realm roles

-----------------------------------------------------------------------------------------------------------

# Keycloak : What is it ?

<br>

Tokens :

  * Access token : in the http request header, provide authorization information


  * Identity token : provide personal information (name...)
      come from OpenID Connect

  * Refresh token : automatic re-authentification (or expiration)

  * Offline session token : to keep offline connection (like refresh token)

  * protocol mappers : allow us to add informations/attributs in tokens (claims)
      client scopes : limit information access

-----------------------------------------------------------------------------------------------------------

# Keycloak : What is it ?

<br>

Identity Provider (IDP) :

  * allows you to authenticate a user 

  * keycloak can use federation (socail network...)