%title: Infra Cloud Infomaniak
%author: xavki


 ██████╗ ██╗████████╗██╗      █████╗ ██████╗ 
██╔════╝ ██║╚══██╔══╝██║     ██╔══██╗██╔══██╗
██║  ███╗██║   ██║   ██║     ███████║██████╔╝
██║   ██║██║   ██║   ██║     ██╔══██║██╔══██╗
╚██████╔╝██║   ██║   ███████╗██║  ██║██████╔╝
 ╚═════╝ ╚═╝   ╚═╝   ╚══════╝╚═╝  ╚═╝╚═════╝ 



-----------------------------------------------------------------------------------------------------------

# Gitlab : Install a docker registry with ansible

<br>

A little fix (locales) Include a new task in our gitlab role

```
- name: Ensure a locale exists
  community.general.locale_gen:
    name: en_US.UTF-8
    state: present
...
- name: include docker registry
  include_tasks: docker_registry.yml
  when: gitlab_docker_registry_enabled
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Install a docker registry with ansible

<br>

Add some variables

```
gitlab_docker_registry_enabled: false
gitlab_docker_registry_all: "false"
gitlab_docker_registry_domain: r.xavki.fr
```

Note : be sure that the external url was in https

-----------------------------------------------------------------------------------------------------------

# Gitlab : Install a docker registry with ansible

<br>

Add a task to reconfigure the systematic registry or not

```
- name: activate or not systematic docker registry
  lineinfile:
    dest: /etc/gitlab/gitlab.rb
    insertafter: "# gitlab_rails.*gitlab_default_projects_features_container_registry.*"
    line: "gitlab_rails['gitlab_default_projects_features_container_registry'] = {{ gitlab_docker_registry_all }}"
  notify: reconfigure_gitlab
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Install a docker registry with ansible

<br>

Change the registry external url

```
- name: set the registry url
  lineinfile:
    dest: /etc/gitlab/gitlab.rb
    insertafter: "# registry_external_url.*"
    line: "registry_external_url 'http://{{ gitlab_docker_registry_domain }}'"
  notify: reconfigure_gitlab
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Install a docker registry with ansible

<br>

Enable registry in the rails code

```
- name: ensure the docker registry is activate
  lineinfile:
    dest: /etc/gitlab/gitlab.rb
    insertafter: "# gitlab_rails.*registry_enabled.*"
    line: "gitlab_rails['registry_enabled'] = true"
  notify: reconfigure_gitlab
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Install a docker registry with ansible

<br>

Define the registry host in the rails code

```
- name: set the registry domain
  lineinfile:
    dest: /etc/gitlab/gitlab.rb
    insertafter: "# gitlab_rails.*registry_host.*"
    line: "gitlab_rails['registry_host'] = '{{ gitlab_docker_registry_domain }}'"
  notify: reconfigure_gitlab
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Install a docker registry with ansible

<br>

Change the API registry domain

```
- name: set the regsitry api url
  lineinfile:
    dest: /etc/gitlab/gitlab.rb
    insertafter: "# gitlab_rails.*registry_api_url.*"
    line: "gitlab_rails['registry_api_url'] = 'http://{{ gitlab_docker_registry_domain }}'"
  notify: reconfigure_gitlab
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Install a docker registry with ansible

<br>

Enable the registry

```
- name: ensure registry is enabled
  lineinfile:
    dest: /etc/gitlab/gitlab.rb
    insertafter: "# registry.*enable.*"
    line: "registry['enable'] = true"
  notify: reconfigure_gitlab
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Install a docker registry with ansible

<br>

Force the registry to listen on all interfaces (not by the nginx proxy)

```
- name: change the registry interface
  lineinfile:
    dest: /etc/gitlab/gitlab.rb
    insertafter: "# registry.*registry_http_addr.*"
    line: "registry['registry_http_addr'] = '0.0.0.0:5000'"
  notify: reconfigure_gitlab
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Install a docker registry with ansible

<br>

Disable the registry in the nginx configuration

```
- name: disable regsitry on nginx
  lineinfile:
    dest: /etc/gitlab/gitlab.rb
    insertafter: "# registry_nginx.*enable.*"
    line: "registry_nginx['enable'] = false"
  notify: reconfigure_gitlab
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Install a docker registry with ansible

<br>

Change the registry listening port in nginx

```
- name: change the listen port
  lineinfile:
    dest: /etc/gitlab/gitlab.rb
    insertafter: "# registry_nginx.*listen_port.*"
    line: registry_nginx['listen_port'] = 5005
    #registry_nginx['listen_https'] = false
  notify: reconfigure_gitlab
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Install a docker registry with ansible

<br>

In our playbook

```
    gitlab_docker_registry_enabled: true
    gitlab_external_url: "https://gl.xavki.fr"
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Install a docker registry with ansible

<br>

And add the docker registry consul service

```
      - {
        name: "gitlab-registry",
        type: "tcp",
        target: "127.0.0.1:5000",
        interval: "10s",
        port: 5000,
        tags: [
          "traefik.enable=true",
          "traefik.http.routers.router-gitlab-registry.entrypoints=http,https",
          "traefik.http.routers.router-gitlab-registry.rule=Host(`r.xavki.fr`)",
          "traefik.http.routers.router-gitlab-registry.service=gitlab-registry",
          "traefik.http.routers.router-gitlab-registry.middlewares=to_https@file",
          "traefik.http.routers.router-gitlab-registry.tls.certresolver=certs_gen"
          ]}
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Install a docker registry with ansible

<br>

Change the gitlab service

```
-          "traefik.http.routers.router-gitlab.middlewares=auth@file,to_https@file,secure_headers@file",
+          "traefik.http.routers.router-gitlab.middlewares=to_https@file,secure_headers@file",
```

-----------------------------------------------------------------------------------------------------------

# Gitlab : Install a docker registry with ansible

<br>

To test it you can remove the Let's Encrypt staging mode to ha a real https