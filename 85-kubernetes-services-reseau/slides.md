%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝




-----------------------------------------------------------------------------------------------------------

# Kubernetes : Services & Network

<br>

Networks with kubernetes :

  * nodes : in your network

  * pods : container ips

  * services : specific load balancer into kubernetes

Pods & Services : need a change into our network terraform implementation


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Services & Network

<br>

Containers local network : (checkout my video on network namespace)

  * bridge

  * veth

  * physical interface


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Services & Network

<br>

CNI manages namespaces :

  * create interfaces

  * create veth

  * setup namespaces

  * add/setup routes

  * configure bridge

  * assing IP

  * create NAT rules

Always with the kubelet help & CRI

List : https://github.com/containernetworking/cni

CNI spec : https://github.com/containernetworking/cni/blob/main/SPEC.md

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Services & Network

<br>

Particularity :

  * each pod have a "pause" container

  * pause container is the owner of the network namespace of the pod ??!!

  * example :
  
      lsns -p xxx

      lsns | awk '$2 ~ "net"{print $0}'

  * by the way : pause container ensures the reaping role (init based)

  
-----------------------------------------------------------------------------------------------------------

# Kubernetes : Services & Network

<br>

How to handle pods ip if IP are dynamic ? (like in docker for example)

How to expose a container ?? 

<br>

Services : how apps are exposed by kube-proxy ?

  * ClusterIP : internal to the cluster

  * NodePort : exposed by a dynamic port on all nodes of the cluster

  * LoadBalancer : automatic external ip allocation (cloud or MetalLB)

  * externalName: map a service with a dns
