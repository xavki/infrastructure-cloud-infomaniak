%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝




-----------------------------------------------------------------------------------------------------------

# Kubernetes : vagrant & manual installation

<br>

Disable swap

```
swapoff -a
sed -ri "s/(.*)swap(.*)/#\1swap\2/g" /etc/fstab
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : vagrant & manual installation

<br>

Install Containerd

```
apt install -y containerd
ls /etc/containerd
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : vagrant & manual installation

<br>

Containerd configuration

```
containerd config default > /etc/containerd/config.toml
sed -ri "s/^(.*)SystemdCgroup = false(.*)$/\1SystemdCgroup = true\2/g" /etc/containerd/config.toml
systemctl restart containerd
```

Note : https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/configure-cgroup-driver/

-----------------------------------------------------------------------------------------------------------

# Kubernetes : vagrant & manual installation

<br>

Check kernel modules

```
ls /etc/modules-load.d/k8s.conf
modprobe overlay
modprobe br_netfilter
echo overlay | sudo tee -a /etc/modules-load.d/k8s.conf
echo br_netfilter | sudo tee -a /etc/modules-load.d/k8s.conf
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : vagrant & manual installation

<br>

Change sysctl parameters

```
echo "net.bridge.bridge-nf-call-iptables  = 1" | sudo tee -a /etc/sysctl.d/k8s.conf
echo "net.bridge.bridge-nf-call-ip6tables  = 1" | sudo tee -a /etc/sysctl.d/k8s.conf
echo "net.ipv4.ip_forward  = 1" | sudo tee -a /etc/sysctl.d/k8s.conf
sysctl --system
```

Note : add bridge to iptables chain + routing packet between pods

-----------------------------------------------------------------------------------------------------------

# Kubernetes : vagrant & manual installation

<br>

Install the repo

```
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.30/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.30/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
```

Note : https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/

-----------------------------------------------------------------------------------------------------------

# Kubernetes : vagrant & manual installation

<br>

Install packages

```
apt-get update
apt-get install -y kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl
systemctl enable --now kubelet
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : vagrant & manual installation

<br>

Initialisation of the first master node

```
kubeadm config images pull
IP="192.168.26.10"
kubeadm init --apiserver-advertise-address=$IP --apiserver-cert-extra-sans=$IP --node-name=kubm1 --pod-network-cidr=10.200.0.0/16  --service-cidr=10.201.0.0/16  --control-plane-endpoint=$IP
```