%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Purposes : 

  - remove swap

  - system customization

  - containerd installation

  - kubectl, kubelet, kubeadm installation


-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Ensure prerequisites packages are installed

```
- name: Install dependencies
  apt:
    name: "{{ kubernetes_prerequisites_packages }}"
    state: present
    update_cache: yes
    cache_valid_time: 3600
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

In defaults

```
kubernetes_prerequisites_packages:
  - apt-transport-https
  - ca-certificates
  - curl
  - gnupg2
  - lsb-release
  - bridge-utils
kubernetes_minor_version: 1.30
kubernetes_version: {{ kubernetes_minor_version }}.3-1.1
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Swap off

```
- name: Disable swap
  shell: swapoff -a
  register: __cmd_res
  changed_when: __cmd_res.rc != 0
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Swap off

```
- name: Remove SWAP in fstab
  lineinfile:
    path: /etc/fstab
    regexp: '(.*)swap(.*)$'
    line: '#\1swap\2'
    backrefs: yes
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Containerd - installation

```
- name: Install containerd
  apt:
    name: containerd
    state: present
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Containerd - check that the directory exists

```
- name: Create containerd config directory before
  file:
    path: /etc/containerd
    state: directory
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Containerd - check the check sum of the config file

```
- name: Get checksum of containerd configuration
  stat:
    path: /etc/containerd/config.toml
  register: __containerd_toml_checksum_before
```

Note : because we don't use a template but need to check a change with before/after


-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Containerd - generate the config file into a variable

```
- name: Generate containerd config file
  shell: containerd config default
  register: __containerd_config
  changed_when: __containerd_config.rc != 0
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Containerd - edit the containerd configuration file

```
- name: Save containerd config file
  copy:
    content: "{{ __containerd_config.stdout }}"
    dest: /etc/containerd/config.toml
  changed_when: __containerd_config.rc != 0
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Containerd - change the configuration file to activate the systemd cgroup mode

```
- name: Change cgroup conf
  lineinfile:
    path: /etc/containerd/config.toml
    regexp: '^(.*)SystemdCgroup = false(.*)$'
    line: '\1SystemdCgroup = true\2'
    backrefs: yes
  changed_when: False
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Containerd - check the checkkksum after

```
- name: Get checksum of containerd configuration after
  stat:
    path: /etc/containerd/config.toml
  register: __containerd_toml_checksum_after
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Containerd - if any change between before/after launch a restart

```
- name: Restart containerd if needed
  service:
    name: containerd
    state: restarted
    enabled: yes
  when: __containerd_toml_checksum_after.stat.checksum != __containerd_toml_checksum_before.stat.checksum
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Kernel modules - ensure if the file exists

```
- name: Check if containerd file exists
  stat:
    path: /etc/modules-load.d/k8s.conf
  register: __containerd_conf_exists
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Kernel modules - ensure modules are installed

```
- name: Add containerd modules
  community.general.modprobe:
    name: "{{ item }}"
    state: present
  loop:
    - overlay
    - br_netfilter
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Kernel modules - create the persistent file if not exists

```
- name: Create k8s.conf file if not exist
  copy:
    dest: /etc/modules-load.d/k8s.conf
    content: |
      overlay
      br_netfilter
  when: not __k8s_conf_exists.stat.exists
  notify: restart_containerd
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Flush handlers to restart containerd if needed

```
- ansible.builtin.meta: flush_handlers
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Ensure containerd is started

```
- name: Ensure containerd is started
  service:
    name: containerd
    state: started
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Ensure sysctl settings

```
- ansible.posix.sysctl:
    name: "{{ item }}"
    value: '1'
    sysctl_set: true
    state: present
    reload: true
  loop:
    - "net.bridge.bridge-nf-call-ip6tables"
    - "net.bridge.bridge-nf-call-iptables"
    - "net.ipv4.ip_forward"
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Ensure sysctl settings

```
- name: add a persisten file for sysctl config
  template:
    src: k8s.conf.j2
    dest: /etc/sysctl.d/99-k8s.conf
    owner: root
    group: root
    mode: 0750
```



-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Repository - check if the gpg key is already installed

```
- name: Check if repo already exists
  stat:
    path: /etc/apt/keyrings/kubernetes-apt-keyring.gpg
  register: __kubernetes_apt_key
```

Note : because templates not used


-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Repository - check if the repo is already installed

```
- name: Check if repo already exists
  stat:
    path: /etc/apt/sources.list.d/kubernetes.list
  register: __kubernetes_apt_source
```

Note : because templates not used


-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Repository - check if the repo is already installed

```
- name: Install the key
  shell: curl -fsSL https://pkgs.k8s.io/core:/stable:/v{{ kubernetes_minor_version }}/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
  when: not __kubernetes_apt_key.stat.exists
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Repository - check if the repo is already installed

```
- name: Install the repo
  shell: "echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v{{ kubernetes_minor_version }}/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list"
  when: not __kubernetes_apt_source.stat.exists
  notify: update_apt_cache
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Repository - install packages

```
- ansible.builtin.meta: flush_handlers

- name: Install Kubernetes packages
  apt:
    name: "{{ item }}"
    state: present
    update_cache: yes
    cache_valid_time: 3600
  loop:
    - "kubelet={{ kubernetes_version }}"
    - "kubeadm={{ kubernetes_version }}"
    - "kubectl={{ kubernetes_version }}"
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Repository - hold kubernetes packages

```
- name: Hold kubernetes packages
  dpkg_selections:
    name: "{{ item }}"
    selection: hold
  loop:
    - kubelet
    - kubeadm
    - kubectl
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Handlers

```
- name: restart_containerd
  service:
    name: containerd
    state: restarted

- name: update_apt_cache
  apt:
    update_cache: yes
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - installation de containerd & binaires

<br>

Template 99-k8s.conf.j2

```
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
```