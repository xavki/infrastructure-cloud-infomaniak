%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - initialisation and first master

<br>

Check if the cluster is already init

```
- name: Check if already init
  stat:
    path: /etc/kubernetes/admin.conf
  register: __kubeadm_already_init
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - initialisation and first master

<br>

Pull images

```
- name: Pull images
  command: "kubeadm config images pull" 
  when: not __kubeadm_already_init.stat.exists
  changed_when: false
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - initialisation and first master

<br>

Run the kubadm init

```
- name: Init cluster
  command: "kubeadm init --apiserver-advertise-address={{ ansible_eth1.ipv4.address }} --apiserver-cert-extra-sans={{ ansible_eth1.ipv4.address }} --node-name={{ ansible_hostname }} --pod-network-cidr=10.200.0.0/16  --service-cidr=10.201.0.0/16  --control-plane-endpoint={{ kube_api_endpoint | default(ansible_eth1.ipv4.address) }}"
  register: __init_output
  when: not __kubeadm_already_init.stat.exists
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - initialisation and first master

<br>

Check if we have a kubeconfig file

```
- name: Check if kubeconfig already exists
  stat:
    path: "{{ ansible_env.HOME }}/.kube"
  register: __kubeconfig_already_init
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - initialisation and first master

<br>

Create the directory if not exists

```
- name: Create .kube directory
  file:
    path: "{{ ansible_env.HOME }}/.kube"
    state: directory
    owner: root
    group: root
    mode: 0750
  when: not __kubeconfig_already_init.stat.exists
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - initialisation and first master

<br>

Copy the kubeconfig file in the root home

```
- name: Copy admin.conf to .kube/config
  copy:
    src: /etc/kubernetes/admin.conf
    dest: "{{ ansible_env.HOME }}/.kube/config"
    remote_src: true
    mode: 0640
  when: not __kubeconfig_already_init.stat.exists
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - initialisation and first master

<br>

Waiting the master is up

```
- name: Check if master is initialized
  uri:
    url: "https://{{ ansible_eth1.ipv4.address }}:6443"
    validate_certs: no
    status_code: [403]
  register: result
  until: "result.status == 403"
  retries: 20
  delay: 5
  when: not __kubeconfig_already_init.stat.exists
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - initialisation and first master

<br>

Launch the kube-router install

```
- name: Add CNI - kuberouter
  shell: kubectl apply -f https://raw.githubusercontent.com/cloudnativelabs/kube-router/master/daemonset/kubeadm-kuberouter.yaml
  when: not __kubeadm_already_init.stat.exists
```

