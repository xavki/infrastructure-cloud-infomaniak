%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - join masters and workers

<br>

First to be more flexible, we can declare : init, masters and workers groups

```
  serial: 1

  vars:
    kubeadm_init_master: kubm1
    kubeadm_master_group_name: k8s-masters
    kubeadm_worker_group_name: k8s-workers
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - join masters and workers

<br>

Then to choose init or join add "when" conditions

```
- name: Initialisation
  ansible.builtin.include_tasks: initialisation.yml
  when: kubeadm_init_master == ansible_hostname

- name: Join server
  include_tasks: join.yml
  when: kubeadm_init_master != ansible_hostname
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - join masters and workers

<br>

Check if we have a cluster :)

```
- name: Check if master is initialized
  delegate_to: "{{ kubeadm_init_master }}"
  uri:
    url: "https://{{ kubeadm_init_master }}:6443"
    validate_certs: no
    status_code: [403]
  register: result
  until: "result.status == 403"
  retries: 20
  delay: 5
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - join masters and workers

<br>

Check if we have a master in ready state

```
- name: Check and wait a master 
  delegate_to: "{{ kubeadm_init_master }}"
  shell: kubectl get nodes | grep " Ready"
  register: __cmd_res
  retries: 100
  delay: 5
  until: __cmd_res.stdout_lines | count > 0
  changed_when: __cmd_res.rc != 0
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - join masters and workers

<br>

Get the node list

```
- name: Check if server already installed is ready
  delegate_to: "{{ kubeadm_init_master }}"
  shell: kubectl get nodes
  register: __cmd_res
  changed_when: __cmd_res.rc != 0
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - join masters and workers

<br>

Get the join command when the hostname is not in the server list
Be careful between delegate node and join nodes

```
- name: Get join command
  delegate_to: "{{ kubeadm_init_master }}"
  command: kubeadm init phase upload-certs --upload-certs
  register: __kubeadm_certs
  when: ansible_hostname not in __cmd_res.stdout
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - join masters and workers

<br>

Get the token

```
- name: Get token command
  delegate_to: "{{ kubeadm_init_master }}"
  command: kubeadm token create --print-join-command
  register: __kubeadm_token
  when: ansible_hostname not in __cmd_res.stdout
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - join masters and workers

<br>

If it's a new worker

```
- name: Joining new worker to master
  shell: "{{ __kubeadm_token.stdout_lines[0] }} --certificate-key {{ __kubeadm_certs.stdout_lines[2] }} --apiserver-advertise-address={{ ansible_eth1.ipv4.address }}"
  when: ansible_hostname not in __cmd_res.stdout and ansible_hostname in groups[kubeadm_worker_group_name]
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : ansible - join masters and workers

<br>

If it's a new worker

```
- name: Joinning new master to master
  shell: "{{ __kubeadm_token.stdout_lines[0] }} --control-plane --certificate-key {{ __kubeadm_certs.stdout_lines[2] }} --apiserver-advertise-address={{ ansible_eth1.ipv4.address }}"
  when: ansible_hostname not in __cmd_res.stdout and ansible_hostname in groups[kubeadm_master_group_name]
```