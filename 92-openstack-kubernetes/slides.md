%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Our issues ??

<br>

Kube-router used BGP to allow each node in the cluster to route to pods and services

Also in our network we could route from any servers to pods and services

To do that we juste need to push our routes on the router RT1

???

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Our issues ??

<br>

Check with a deployment from :

  * our worker node

  * our master node

  * another node


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Our issues ??

<br>

We need to do some changes :

  * allow address pairs : 
      Networks > internal_dev > Ports > xxx

    Remember this is on the port of each workers !! > change our module

  * add services and pods routes to our router

    but we need to have a static ip !! > use our module option (instance_internal_fixed_ip)

  * dynamic dns with consul > consul catalog