%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Fix issue & Improvements part1

<br>

What we need to do :

  * fix the coredns issue

  * fix high availability with the control plane issue

  * change our instance module

  * add static routes

  * SPOF : different routing solutions & availability zones


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Fix issue & Improvements part1

<br>

* fix coredns issues - security group rules

```
resource "openstack_networking_secgroup_v2" "k8s_internal_pods" {
  name        = "k8s_internal_pods"
  description = "k8s_internal_pods"
}
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Fix issue & Improvements part1

<br>

* fix coredns issues - security group rules

```
resource "openstack_networking_secgroup_rule_v2" "secgroup_k8s_internal_pods_rule_tcp_v4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 1
  port_range_max    = 65535
  remote_ip_prefix  = "10.200.0.0/16"
  security_group_id = openstack_networking_secgroup_v2.k8s_internal_pods.id
  lifecycle {
    ignore_changes = [port_range_min, port_range_max]
  }
}
```


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Fix issue & Improvements part1

<br>

* fix coredns issues - security group rules

```
resource "openstack_networking_secgroup_v2" "k8s_internal_services" {
  name        = "k8s_internal_services"
  description = "k8s_internal_services"
}
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Fix issue & Improvements part1

<br>

* fix coredns issues - security group rules

```
resource "openstack_networking_secgroup_rule_v2" "secgroup_k8s_internal_services_rule_tcp_v4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 1
  port_range_max    = 65535
  remote_ip_prefix  = "10.201.0.0/16"
  security_group_id = openstack_networking_secgroup_v2.k8s_internal_services.id
  lifecycle {
    ignore_changes = [port_range_min, port_range_max]
  }
}
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Fix issue & Improvements part1

<br>

* fix coredns issues - remove loop paramater

add new template to remove loop

add new handlers

```
- name: fix the coredns loop issue
  template:
    src: configmap-coredns-fix.yml.j2
    dest: /etc/kubernetes/configmap-coredns-fix.yml
    owner: root
    group: root
    mode: 0750
  notify: apply_coredns_configmap
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Fix issue & Improvements part1

<br>

* fix coredns issues - remove loop paramater

```
  - name: apply_coredns_configmap
    ansible.builtin.shell: kubectl apply -n kube-system -f /etc/kubernetes/configmap-coredns-fix.yml
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Fix issue & Improvements part1

<br>

* fix high availability - change the control plane endpoint

```
- name: Init cluster
  ansible.builtin.command: "kubeadm init --apiserver-advertise-address={{ ansible_default_ipv4.address }} --apiserver-cert-extra-sans={{ kubernetes_api_cert_sans | default(ansible_default_ipv4.address) }} --node-name={{ ansible_hostname }} --pod-network-cidr={{ kubernetes_cidr_pods }}  --service-cidr={{ kubernetes_cidr_services }} --control-plane-endpoint={{ kubernetes_api_endpoint | default(ansible_default_ipv4.address) }}"
  register: __init_output
  when: not __kubeadm_already_init.stat.exists
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Fix issue & Improvements part1

<br>

* fix high availability - change the control plane endpoint

```
consul/consul_services

    consul_services:
      - {
        name: "k8s-apiserver",
        type: "tcp",
        check_target: "127.0.0.1:6443",
        interval: "10s",
        port: 6443      
        }
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Fix issue & Improvements part1

<br>

* fix high availability - change the control plane endpoint


```
    kubernetes_api_endpoint: "k8s-apiserver.service.consul"
    kubernetes_api_cert_sans: "{{ kubernetes_api_endpoint }}"
```