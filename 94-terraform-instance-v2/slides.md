%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Instance module improvements

<br>

What we need to do ?

  * Change module to add port management

  * Add routes on the main router

Note: not the best practice to develop terraform moddule (we'll see it later)


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Instance module improvements

<br>

To change the instance management and add ports :

  * to add allowed address > need to attribute port to instance

  * to create openstack_networking_port_v2 > need to have network and subnet ids

  * the internal ip can be fix or random

  * the secgroup need to be push on the port (and we can keep ip on the instance)

  * to add security group on port we need to have their ids (one or many)

  * we need to have one or many allowed address pairs that we need to use dynamic bloc in terraform

  * of course need to have one port per instance (multiple instance in same module)

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Instance module improvements

<br>

First, we can start by the content of a port resource

```
resource "openstack_networking_port_v2" "internal_port" {
  count      = var.instance_count
  name       = "internal-${var.instance_name}${count.index + 1}"
  network_id = data.openstack_networking_network_v2.network.id
  port_security_enabled = true
  security_group_ids = local.secgroup_list
  fixed_ip   {
    subnet_id = data.openstack_networking_subnet_v2.subnet.id 
    ip_address = var.instance_internal_fixed_ip == "" ? "" : "${var.instance_internal_fixed_ip}${count.index + 1}"
  }
  dynamic "allowed_address_pairs" {
    for_each = length(var.allowed_addresses) == 0 ? [] : var.allowed_addresses
    content {
      ip_address = allowed_address_pairs.value
    }
  }
}
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Instance module improvements

<br>

Then before to create the port, we need to get some ids

```
data "openstack_networking_network_v2" "network" {
  name = var.instance_network_internal
}
```

```
data "openstack_networking_subnet_v2" "subnet" {
  name = var.instance_network_internal
}
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Instance module improvements

<br>

Then we collect ids of security group with a for each loop

```
data "openstack_networking_secgroup_v2" "sg" {
  for_each = toset(var.instance_security_groups)
  name = "${each.key}"
}
```

But wee need to have it as a list format

```
locals {
  secgroup_list = [ for sg in data.openstack_networking_secgroup_v2.sg: sg.id ]
}
```


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Instance module improvements

<br>

Get port ids

```
data openstack_networking_port_v2 "port" {
  count      = var.instance_count
  name       = "internal-${var.instance_name}${count.index + 1}"

  depends_on = [ openstack_networking_port_v2.internal_port ]
}
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Instance module improvements

<br>

In variables add

```
variable "allowed_addresses" {
  type = list
  default = []
}
```