%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Instance module improvements - part2

<br>

Change openstack_networking_floatingip_v2 pool usage with datasource

```
resource "openstack_networking_floatingip_v2" "floatip_1_random" {
  count = var.public_floating_ip  && var.public_floating_ip_fixed == "" ? var.instance_count : 0
  #pool       = var.instance_network_external_name
  #subnet_ids = data.openstack_networking_subnet_ids_v2.ext_subnets[0].ids
  
  ## new
  pool       = data.openstack_networking_network_v2.network[count.index].name
  subnet_ids = data.openstack_networking_subnet_ids_v2.ext_subnets[count.index].ids

}
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Instance module improvements - part2

<br>

Change the instance module : security groups, network

```
resource "openstack_compute_instance_v2" "instance" {
  count           = var.instance_count
  name            = "${var.instance_name}${count.index + 1}"
  image_id        = var.instance_image_id
  flavor_name     = var.instance_flavor_name
  metadata        = var.metadatas
  user_data = data.template_file.userdata.rendered
  security_groups = var.instance_security_groups
  key_pair        = var.instance_key_pair

  network {
    port = data.openstack_networking_port_v2.port[count.index].id
  }
  depends_on = [ openstack_networking_port_v2.internal_port ]
}
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Instance module improvements - part2

<br>

Then into the instance usage, add

```
instance_internal_fixed_ip = "10.0.1.10"

allowed_addresses           = ["10.200.0.0/16","10.201.0.0/16"]
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Instance module improvements - part2

<br>

And finally to add new static routes

```
locals {
  k8s_masters = ["10.0.1.101","10.0.1.102","10.0.1.103"]
}
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Instance module improvements - part2

<br>

And finally to add new static routes

```
resource "openstack_networking_router_route_v2" "k8s_pods_routes" {
  for_each = toset(local.k8s_masters)
  router_id        = openstack_networking_router_v2.rt1.id
  destination_cidr = "10.200.0.0/16"
  next_hop         = "${each.key}"
  depends_on       = [openstack_networking_router_v2.rt1]
}
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Instance module improvements - part2

<br>

And finally to add new static routes

```
resource "openstack_networking_router_route_v2" "k8s_services_routes" {
  for_each = toset(local.k8s_masters)
  router_id        = openstack_networking_router_v2.rt1.id
  destination_cidr = "10.201.0.0/16"
  next_hop         = "${each.key}"
  depends_on       = [openstack_networking_router_v2.rt1]
}
```