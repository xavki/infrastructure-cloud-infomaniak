%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Consul Sync catalog

<br>

How to create to resolv pods or services from instances ?

  Consul Sync Catalog

  Kubernetes internal dns <> Consul nodes/services

That provides us a dns with an automatic synchronization

AND we keed the auto configuration of our traefik ;)


add security group


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Consul Sync catalog

<br>

Just a little fix ;)

```
forward . {{ groups[kubernetes_consul_group] | map('extract', hostvars, ['ansible_host']) | join(' ') }} {
  max_concurrent 1000
}
cache 100
loop
```

Add 10.200.0.0/16 in all internal for UDP 53

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Consul Sync catalog

<br>


But how to deploy in kubernetes... with helm and ansible

So create a new role ;)

```
kubernetes_addons_helm_version: 3.13.3

- name: prerequisites
  apt:
    name: python3-yaml
    state: present
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Consul Sync catalog

<br>

* Then install helm

```
- name: Install helm if not exists
  unarchive:
    src: "https://get.helm.sh/helm-v{{ kubernetes_addons_helm_version }}-linux-amd64.tar.gz"
    dest: /usr/local/bin
    extra_opts: "--strip-components=1"
    owner: root
    group: root
    mode: 0755
    remote_src: true
  args:
    creates: /usr/local/bin/helm
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Consul Sync catalog

<br>

* And install the hashicorp repo

```
- name: Add stable chart repo
  kubernetes.core.helm_repository:
    name: hashicorp
    repo_url: "https://helm.releases.hashicorp.com"
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Consul Sync catalog

<br>

* Finally deploy consul with a values file

```
- name: Deploy consul sync
  kubernetes.core.helm:
    name: consul
    chart_ref: hashicorp/consul
    #chart_version: 0.22.0
    chart_version: 0.45.0
    release_namespace: sre
    create_namespace: true
    values: "{{ lookup('template', 'consulsync-values.yml') | from_yaml }}"
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Consul Sync catalog

<br>

* And add the template and join the cluster

```
global:
  enabled: false
  datacenter: xavki
syncCatalog:
  addK8SNamespaceSuffix: false
  default: true
  enabled: true
  syncClusterIPServices: true
  toConsul: true
  toK8S: false
  k8sAllowNamespaces: ['default']
  securityContext:
    runAsNonRoot: false
client:
  enabled: true
  join:
{% for srv in groups['meta-app_consul'] %}
    - {{ hostvars[srv]['ansible_host'] }}
{% endfor %}
```