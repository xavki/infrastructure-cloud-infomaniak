%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Use annotations for consul services

<br>

How to create to resolv pods or services from instances ?

  Consul Sync Catalog

  Kubernetes internal dns <> Consul nodes/services

That provides us a dns with an automatic synchronization

AND we keed the auto configuration of our traefik ;)



-----------------------------------------------------------------------------------------------------------

# Kubernetes : Use annotations for consul services

<br>

```
  annotations:
    consul.hashicorp.com/service-name: hello-infomaniak
    consul.hashicorp.com/service-sync: "true"
    consul.hashicorp.com/service-tags: |
      traefik.enable=true,
      traefik.http.routers.router-hello-test.entrypoints=http\,https,
      traefik.http.routers.router-hello-test.rule=Host(`i.xavki.fr`),
      traefik.http.routers.router-hello-test.service=hello-infomaniak,
      traefik.http.routers.router-hello-test.middlewares=secure_headers@file\,to_https@file,
      traefik.http.routers.router-hello-test.tls.certresolver=certs_gen
```