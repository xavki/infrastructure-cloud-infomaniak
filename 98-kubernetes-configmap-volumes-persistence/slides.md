%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

Purpose :

  - configmap

  - secrets

  - PV, PVC & Storage Class

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

How to store configuration in kubernetes ? just key/value or entire file

Two resources :

  - configmap

  - secrets

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

For a configmap

```
kubectl create configmap langue --from-literal=LANGUAGE=Fr
kubectl get configmaps
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

For a secret

```
kubectl create configmap podenv --from-literal=LANGUAGE=Fr --from-literal=ENCODING=UTF-8
kubectl get configmaps
```

Caution : a secret in CLI is not a good idea (add space if you really want to do it)


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

Configmap from a file

```
kubectl create configmap podenv --from-file=config.cfg
kubectl get configmaps
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

How to use it ?

If you need to set an environment variable

```
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: test-container
      image: k8s.gcr.io/busybox
      command: [ "/bin/sh", "-c", "env" ]
      env:
        - name: LANGUAGE
          valueFrom:
            configMapKeyRef:
              name: podenv
              key: LANGUAGE
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

Or if you want to use all the configmap

```
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: test-container
      image: busybox
      command: [ "/bin/sh", "-c", "env" ]
      envFrom:
        - configMapRef:
            name: podenv
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

If we want to add a entire config file

```
kind: ConfigMap
apiVersion: v1
metadata:
  name: configpod
data:
  config.yml: |
    version: 1.2
    env: prod
    connection: url://127.0.0.1
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

If we want to add a entire config file

```
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: test-container
      image: busybox
      command: [ "cat","/etc/test/config.yml"]
      volumeMounts:
      - mountPath: /etc/test/
        name: myvolume
  volumes:
  - name: myvolume
    configMap:
      name: configpod
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

If multiple files

```
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: test-container
      image: busybox
      command: [ "ls","/etc/test/"]
      volumeMounts:
      - mountPath: /etc/test/
        name: myvolume
  volumes:
  - name: myvolume
    configMap:
      name: configpod
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

If select in multiple files

```
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: test-container
      image: busybox
      command: [ "cat","/etc/test/settings.yml"]
      volumeMounts:
      - mountPath: /etc/test/
        name: myvolume
  volumes:
  - name: myvolume
    configMap:
      name: configpod
      items:
      - key: config2.yml
        path: settings.yml
```


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

For secrets

```
volumes:
  - name: secret-volume
    secret:
      secretName: test-secret
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

or

```
volumes:
- name: foo
  secret:
    secretName: mysecret
    items:
    - key: username
      path: my-group/my-username
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

or

```
- name: DB_USERNAME
  valueFrom:
    secretKeyRef:
      name: db-user
      key: db-username
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

Persistence of data is managed by : 
- persistence volumes
- persistence volume claims
- storage classes (optionnal)
- or directly

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

To mount a host volume

```
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: test-container
      image: busybox
      command: [ "cat","/etc/test/hosts"]
      volumeMounts:
      - mountPath: /etc/test/
        name: etchosts
  volumes:
    - name: etchosts
      hostPath:
        path: /etc/
```

Caution : if the pod changes server

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

Create a PV

```
kind: PersistentVolume
apiVersion: v1
metadata:
  name: pv1
  labels:
    type: local
spec:
  storageClassName: manual
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/pvdata"
```

Note : before create /pvdata on the worker node

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

Create a PVC

```
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: pvc1
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

And to test the mount volume

```
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: test-container
      image: busybox
      command: ["/bin/sh", "-c", "while true;do echo 1 >> /etc/test/worker.done;sleep 1;done"]
      volumeMounts:
      - mountPath: /etc/test/
        name: worker
  volumes:
    - name: worker
      persistentVolumeClaim:
       claimName: pvc1
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

But for individual worker it's ok but data is not shared between nodes

Example with external nfs storage

```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv1
spec:
  storageClassName: alertmanager
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  nfs:
    server: 192.168.12.20
    path: "/srv/monitoring/prometheus/"
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

And to test the mount volume

```
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: pvc1
spec:
  storageClassName: alertmanager
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Configmaps, Secrets & Data Persistence

<br>

So Storage Class is very important to use CSI driver

https://kubernetes.io/docs/concepts/storage/persistent-volumes/
