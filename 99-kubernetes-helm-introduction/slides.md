%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm introduction

<br>

Helm = gouvernail

Github https://github.com/helm/helm
Site : https://helm.sh/

Templating and Packaging Tool

Language : Golang


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm introduction

<br>

How to deploy and version a complete stack on kubernetes ?

  * many resources to create

  * share some variables & define templates

  * hook concepts : pre-installation, post-installlation...

  * centralize all code to deploy many stacks

  * one chart can orchestrate many charts


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm introduction

<br>

Definition & Concepts :

  * chart : all code and templates to deploy a stack
      version of the chart vs version of tools into the stack

  * release : one instance of the chart, 1 deployment of the stack with a specific name and variables

  * registry : where to store stacks (chart also can be stored in a simple git repo)

  * helm release : when you use helm operator

  * values : file where default variables are stored in values.yaml

  * appVersion : version of the stack tool

  * version : version of the chart

appVersion and Version are disconnected (example : change of config file or no)

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm introduction

<br>

Some directories

* Configuration directory

$HOME/.config/helm/

* Cache directory

$HOME/.cache/helm/

* data directory
  
$HOME/.local/share/helm/


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm introduction

<br>

Helm installation

```
curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
```


-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm introduction

<br>


By default Helm uses kubeconfig file and your context

--kubeconfig : localisation du kubeconfig

--namespace, -n : namespace de helm

--kube-context : contexte à utiliser

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Helm introduction

<br>

How to create a chart ?


```
helm create xavki
```

Directories & Files : 

  * Chart.yaml : versions and dependacies

  * values.yaml : default variable settings

  * charts : the charts on which the chart depends

  * templates : all resource templates
  