variable "instance_name" {
  type = string
}

variable "instance_image_id" {
  type = string
}

variable "instance_flavor_name" {
  type = string
}

variable "instance_ssh_key" {
  type = string
}

variable "instance_key_pair" {
  type = string
}

variable "instance_count" {
  type = number
  default = 1
}

variable "instance_security_groups" {
  type = list
}

variable "allowed_addresses" {
  type = list
  default = []
}

variable "instance_network_name" {
  type = string
}

variable "public_floating_ip" {
  type = bool
  default = false
}

variable "ext_network" {
  type = string
  default = "ext-floating1"
}

variable "metadatas" {
  type = map(string)
  default = {
    "environment" = "dev"
  }
}

variable "instance_volumes_count" {
  type = number
  default = 0
}

variable "instance_volumes_size" {
  type = number
  default = 20
}

variable "instance_volumes_type" {
  type = string
  default = "CEPH_1_perf1"
}

variable "instance_internal_fixed_ip" {
  type = string
  default = ""
}

variable "public_floating_ip_fixed" {
  type = string
  default = ""
}

